#!/usr/bin/env python

from distutils.core import setup

setup(
    name='ddd',
    version='0.1.0',
    author='Anonymized for submission',
    author_email='Anonymized for submission',
    packages=['ddd'],
    url='https://gitlab.com/anon_user1/dsaa2020',
    license='LICENSE',
    description='DSAA 2020 anonymized submission',
    long_description=open('README.md').read(),
    install_requires=['pandas', 'scipy', 'numpy'],
)
